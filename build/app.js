class Game {
    constructor(canvasId) {
        this.player = "Player1";
        this.score = 400;
        this.lives = 3;
        this.canvas = canvasId;
        this.canvas.width = window.innerWidth;
        this.canvas.height = window.innerHeight;
        this.ctx = this.canvas.getContext("2d");
        this.highscores = [
            {
                playerName: "Loek",
                score: 40000
            },
            {
                playerName: "Daan",
                score: 34000
            },
            {
                playerName: "Rimmert",
                score: 200
            }
        ];
    }
    start_screen() {
        this.writeAsteroidsToDOM();
        this.writePlayTextToDOM();
        this.writeStartButton();
        this.drawAsteroid();
    }
    clearScreen() {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }
    writeAsteroidsToDOM() {
        this.writeTextToCanvas(100, "#FFF", "center", "Asteroids", this.canvas.width / 2, this.canvas.height / 3);
    }
    writePlayTextToDOM() {
        this.writeTextToCanvas(70, "#FFF", "center", "Press play to start", this.canvas.width / 2, this.canvas.height / 2);
    }
    drawAsteroid() {
        let asteroid = new Image();
        asteroid.src = "assets/images/SpaceShooterRedux/PNG/Meteors/meteorBrown_big4.png";
        asteroid.onload = () => {
            this.ctx.drawImage(asteroid, this.canvas.width / 2 - 40, this.canvas.height / 2 + 50);
        };
    }
    writeStartButton() {
        let startButton = new Image();
        startButton.id = "startButton";
        startButton.src = "assets/images/SpaceShooterRedux/PNG/UI/buttonBlue.png";
        startButton.onload = () => {
            this.ctx.drawImage(startButton, this.canvas.width / 2 - 100, this.canvas.height / 2 + 200);
            this.writeTextToCanvas(30, "#000", "center", "Play", this.canvas.width / 2 + 10, this.canvas.height / 2 + 230);
            this.canvas.addEventListener("click", (evt) => this.eventHandler(evt));
        };
    }
    eventHandler(event) {
        let buttonX = this.canvas.width / 2 - 100;
        let buttonY = this.canvas.height / 2 + 200;
        let buttonW = 220;
        let buttonH = 35;
        if (event.x > buttonX &&
            event.x < buttonX + buttonW &&
            event.y > buttonY &&
            event.y < buttonY + buttonH) {
            this.level_screen();
        }
    }
    level_screen() {
        this.clearScreen();
        this.writeLivesToDOM();
        this.writeScoreToLevelScreen();
        this.drawRandomAsteroids();
        this.writeShipToDOM();
    }
    writeShipToDOM() {
        let ship = new Image();
        ship.src = "assets/images/SpaceShooterRedux/PNG/playerShip3_blue.png";
        ship.onload = () => {
            this.ctx.drawImage(ship, this.canvas.width / 2 - 50, this.canvas.height / 2);
        };
    }
    writeLivesToDOM() {
        let lifeCounter = 3;
        let widthCounter = 0;
        let lifeImg = new Image();
        lifeImg.src = "assets/images/SpaceShooterRedux/PNG/UI/playerLife3_blue.png";
        lifeImg.onload = () => {
            for (let index = 0; index < lifeCounter; index++) {
                this.ctx.drawImage(lifeImg, 10 + widthCounter, this.canvas.height / 2 - 300);
                widthCounter += 35;
            }
        };
    }
    writeScoreToLevelScreen() {
        this.writeTextToCanvas(15, "#FFF", "center", "Your score: 400", this.canvas.width - 90, this.canvas.height / 2 - 285);
    }
    drawRandomAsteroids() {
        let allAsteroids = ["big1", "big2", "big3", "big4", "med1", "med2", "small1", "small2", "tiny1", "tiny2"];
        let randomAmountOfAsteroids = this.randomNumber(1, 6);
        for (let index = 0; index < randomAmountOfAsteroids; index++) {
            let random = allAsteroids[Math.floor(Math.random() * allAsteroids.length)];
            let asteroidImage = new Image();
            asteroidImage.src = `./assets/images/SpaceShooterRedux/PNG/Meteors/meteorBrown_${random}.png`;
            asteroidImage.addEventListener('load', () => {
                let randomX = this.randomNumber(100, this.canvas.width - 100);
                let randomY = this.randomNumber(100, this.canvas.height - 100);
                this.ctx.drawImage(asteroidImage, randomX, randomY);
            });
        }
    }
    title_screen() {
        this.writeOwnScoreToDOM();
        this.writeHighScoresToDOM();
    }
    writeOwnScoreToDOM() {
        this.writeTextToCanvas(85, "#FFF", "center", "Your score is 400", this.canvas.width / 2, this.canvas.height / 2 - 100);
    }
    writeHighScoresToDOM() {
        let counter = 0;
        let heightCounter = 50;
        for (let index = 0; index < this.highscores.length; index++) {
            counter++;
            let playerNames = this.highscores[index].playerName;
            let playerScores = this.highscores[index].score;
            let fullHighScore = `${counter}: ${playerNames} - ${playerScores}`;
            this.writeTextToCanvas(25, "#FFF", "center", fullHighScore, this.canvas.width / 2, this.canvas.height / 2 - 50 + heightCounter);
            heightCounter += 50;
        }
    }
    writeTextToCanvas(fontsize, color, alignment, text, xcoordinate, ycoordinate) {
        this.ctx.fillStyle = color;
        this.ctx.font = `${fontsize}px Minecraft`;
        this.ctx.textAlign = alignment;
        this.ctx.fillText(text, xcoordinate, ycoordinate);
    }
    randomNumber(min, max) {
        return Math.round(Math.random() * (max - min) + min);
    }
}
let init = function () {
    const Asteroids = new Game(document.getElementById("canvas"));
};
window.addEventListener("load", init);
//# sourceMappingURL=app.js.map